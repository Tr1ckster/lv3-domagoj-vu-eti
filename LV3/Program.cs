﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV3
{
    class Program
    {
        static void Main(string[] args)
        {
            //Zadatak1
            Dataset data1 = new Dataset("csv.txt");
            Dataset data2 = (Dataset)data1.Clone();
            data1.Print();
            data2.Print();
            //Duboko kopiranje koristimo kada želimo mijenjati klon, a da orginal ostane isti,
            //ako koristimo plitko kopiranje mijenjanjem klona promijeniti će se i orginal

            //Zadatak2
            RandomGenerator matrica = RandomGenerator.GetInstance();
            matrica.Set(7, 5);
            matrica.fillMatrix();
            matrica.printMatrix();

            //Zadatak3
            Logger logger = Logger.GetInstance();
            logger.Log("Test 1 2 1 2 3");

            //Zadatak4
            ConsoleNotification notification = new ConsoleNotification("Vučetić", "RPPOON - LV 3", "Ovo je Zadatak 4", DateTime.Now, Category.INFO, ConsoleColor.DarkMagenta);
            NotificationManager manager = new NotificationManager();
            manager.Display(notification);
        }
    }
}
