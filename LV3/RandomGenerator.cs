﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV3
{
    class RandomGenerator
    {
        private static RandomGenerator instance;
        private Random generator = new Random();
        private double[,] matrix;
        private int m, n;
        
        public static RandomGenerator GetInstance()
        {
            if (instance == null)
            {
                instance = new RandomGenerator();
            }
            return instance;
        }
        private RandomGenerator()
        {
            this.matrix = new double[1, 1];
        }
        public void Set(int m, int n)
        {
            this.m = m;
            this.n = n;
            this.matrix = new double[m, n];
        }
        public void fillMatrix()
        {
            int i, j;
            for (i = 0; i < m; i++)
            {
                for (j = 0; j < n; j++)
                {
                    this.matrix[i, j] = generator.NextDouble();
                }
            }
        }
        public void printMatrix()
        {
            int i, j;
            for (i = 0; i < m; i++)
            {
                Console.WriteLine();
                for (j = 0; j < n; j++)
                {
                    Console.Write(matrix[i, j] + " ");
                }
            }
            Console.WriteLine();
        }
    }
}


